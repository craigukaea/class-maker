from ClassMaker import ClassMaker


if __name__ == "__main__":

    maker = ClassMaker()
    maker.setClassName('Foo')
    maker.addFunction('bar', lambda : print("Hello"))
    maker.addFunctionToInitialiser(lambda x: x.bar())
    
    foo = maker.makeClass()
    foo.__init__()