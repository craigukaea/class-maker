from ClassMaker import ClassMaker
    
if __name__ == "__main__":

    maker = ClassMaker()
    maker.setClassName('Foo')
    maker.addFunction('bar', lambda : print("Hello World!"))
    maker.addFunction_1arg('echo', lambda arg : print(arg))
    maker.addFunction_2arg("sum", lambda arg1, arg2 : print(arg1 + arg2))
    maker.addFunction('ret42', lambda : 42)
    maker.addVariable('baz', 5)
    
    maker.addFunctionToInitialiser(lambda x: print("Initialising"))
    maker.addFunctionToInitialiser(lambda x: print("!"))

    foo = maker.makeClass()
    foo.__init__(foo)
    foo.bar()
    foo.echo(7)
    foo.sum(9,1)
    print(foo.ret42())
    print(foo.baz)