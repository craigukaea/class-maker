from ClassMaker import ClassMaker

class A:
    def b(self):
        print("Hello World!")
    
if __name__ == "__main__":

    maker = ClassMaker()
    maker.setClassName('Foo')
    maker.addVariable('a', A())
    
    foo = maker.makeClass()
    foo.__init__(foo)
    foo.a.b()