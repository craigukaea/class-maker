import setuptools
with open("README.md", "r") as fh:
    long_description = fh.read()
    
setuptools.setup(
     name='ClassMaker',  
     version='0.1',
     author="Craig Hickman",
     author_email="craig.hickman@ukaea.uk",
     description="A python module to help with meta classes",
     long_description=long_description,
     long_description_content_type="text/markdown",
     packages=setuptools.find_packages(),
     classifiers=[
         "Programming Language :: Python :: 3",
         "License :: TBD OSI Approved :: TBD License",
         "Operating System :: OS Independent",
     ]
 )
